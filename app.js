// Loading the dependencies.
const axios = require("axios");
const cheerio = require("cheerio");
const { log } = require("console");
const { MongoClient } = require("mongodb");
const express = require("express");
const cron = require("node-cron");
const app = express();
const port = 3000;
let appStartTime = new Date();
let isRunOnce = false;
let lastScrapperRun = new Date();
let currentPageMapper = new Map();
// URL of the page we want to scrape
const url = "https://thenextweb.com";
// ...
const uriOfMongo =
  "mongodb://prospect-plotter-news-db:8bcHk6OPZsWymvNp3DC2fKMzZwANT0LBMi9P5SuhwFEs94M1NMgfhJrJrXl0cxgfjl3VuhGY6cIHDgjAuQ0RJw%3D%3D@prospect-plotter-news-db.mongo.cosmos.azure.com:10255/?ssl=true&retrywrites=false&maxIdleTimeMS=120000&appName=@prospect-plotter-news-db@";
let timeOutDuration = function getRandomTimeOut() {
  // log(`Random Delay Generated --- ${1000 + Math.floor(Math.random() * 1000)}`);
  return 10000 + Math.floor(Math.random() * 10000);
};
let databaseName = "world-news-scraping";
let newsCollection = "news";
let newsErrorCollection = "news-errors";

// Schedule tasks to be run on the server.
cron.schedule("0 1 * * *", function () {
  console.log(`Running News Scraping Again -- ${new Date()}`);
  if (isRunOnce) {
    lastScrapperRun = new Date();
    log(`Running Periodic Latest News Scrapper Only`);
    scrapeDataForLatestNewsOnly();
  }
});

app.get("/", (req, res) => {
  let health = new Object();
  health.status = "UP";
  health.time = new Date();
  health.details = "News Scrapper for Url : https://thenextweb.com";
  health.lastScrapperRun = lastScrapperRun;
  health.port = port;
  health.appStartTime = appStartTime;
  console.log(currentPageMapper);
  health.currentPageMapper = Object.fromEntries(currentPageMapper);
  if (!isRunOnce) {
    log(`Running Main Scrapper for the First time Only`);
    isRunOnce = true;
    scrapeDataForInitialNews();
  }
  res.send(JSON.stringify(health));
});

app.listen(port, () => {
  log(`Example app listening on port ${port}`);
});

async function saveDataToSotrageAzureCosmos(
  newsPayload,
  databaseName,
  collectionName
) {
  //log(`INIT TO THE CLIENT - ${databaseName} [${collectionName}]`);
  const client = new MongoClient(uriOfMongo);
  try {
    // log("CONNECTING TO THE CLIENT");
    await client.connect();
    const database = client.db(databaseName);
    const news = database.collection(collectionName);

    //Find of Data is Already available - then Just Update It or Insert It
    // Query for a News Via URL
    const query = { sourceUrl: newsPayload.sourceUrl };
    const options = {
      // sort matched documents in descending order by rating
      sort: { _id: -1 },
      // Include only the `_id` and `sourceUrl` fields in the returned document
      projection: { _id: 1, sourceUrl: 1, failedUrl: 1, failStatusCode: 1 },
    };
    const newsInDB = await news.findOne(query, options);
    if (newsInDB) {
      log(
        `News Report/Error Already present in Database --------- ${JSON.stringify(
          newsInDB
        )}`
      );
    } else {
      const result = await news.insertOne(newsPayload);
      log(
        `A document [ ${JSON.stringify(
          newsPayload
        )}] was inserted with the Payload--------------: ${JSON.stringify(
          result
        )}`
      );
    }
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
    // log("CLOSING THE CLIENT");
  }
}

// Async function which scrapes the data
async function scrapeDataForLatestNewsOnly() {
  try {
    let childUrlsForParsing = [];
    let brokenLinkPropagationAllowed = 15;
    let allowedLinkHeaders = new Set([
      "Latest",
      "Gadgets & apps",
      "AI & futurism",
      "EVs & mobility",
      "Startups & growth",
      "Crypto & fintech",
      "Careers in tech",
      "Future of Finance",
      "Read Me",
    ]);
    // Fetch HTML of the page we want to scrape
    await sleep(timeOutDuration());
    const { data } = await axios.get(url, {
      timeout: timeOutDuration(),
    });
    // Load HTML we fetched in the previous line
    const $ = cheerio.load(data);
    const headerUrls = $("a");
    // log(
    //   `The allowed Sites to Scrape : ${Array.from(allowedLinkHeaders)}`
    // );
    headerUrls.each(function (idx, el) {
      if (allowedLinkHeaders.has($(el).text())) {
        childUrlsForParsing.push(url + $(el).attr("href"));
      }
    });
    //childUrlsForParsing = ["https://thenextweb.com/latest"]; -- for testing purpose
    // log(`The Sub Urls - ${childUrlsForParsing}`);
    for (let element of childUrlsForParsing) {
      // log(`Parsing the Site - ${element}`);

      findNesHeadlinesRecursively(
        element,
        1,
        brokenLinkPropagationAllowed,
        0,
        true
      );
    }
  } catch (err) {
    // log(
    //   "Not Able to Process Headline Parser So Storing for Retry Later ====> " +
    //     url
    // );
    if (err.response && err.response.status >= 400) {
      let newsObject = new Object();
      newsObject.failedUrl = url;
      newsObject.failedUrlType = "MAIN-PAGE";
      newsObject.failStatusCode = err.response.status;
      saveDataToSotrageAzureCosmos(
        newsObject,
        databaseName,
        newsErrorCollection
      );
    }
  }
}

// Async function which scrapes the data
async function scrapeDataForInitialNews() {
  try {
    let childUrlsForParsing = [];
    let brokenLinkPropagationAllowed = 15;
    let allowedLinkHeaders = new Set([
      "Latest",
      "Gadgets & apps",
      "AI & futurism",
      "EVs & mobility",
      "Startups & growth",
      "Crypto & fintech",
      "Careers in tech",
      "Future of Finance",
      "Read Me",
    ]);
    // Fetch HTML of the page we want to scrape
    await sleep(timeOutDuration());
    const { data } = await axios.get(url, {
      timeout: timeOutDuration(),
    });
    // Load HTML we fetched in the previous line
    const $ = cheerio.load(data);
    const headerUrls = $("a");
    // log(
    //   `The allowed Sites to Scrape : ${Array.from(allowedLinkHeaders)}`
    // );
    headerUrls.each(function (idx, el) {
      if (allowedLinkHeaders.has($(el).text())) {
        childUrlsForParsing.push(url + $(el).attr("href"));
      }
    });
    // log(`The Sub Urls - ${childUrlsForParsing}`);
    for (let element of childUrlsForParsing) {
      log(`Parsing the Site - ${element}`);

      findNesHeadlinesRecursively(
        element,
        1,
        brokenLinkPropagationAllowed,
        0,
        false
      );
    }
  } catch (err) {
    // log(
    //   "Not Able to Process Headline Parser So Storing for Retry Later ====> " +
    //     url
    // );
    if (err.response && err.response.status >= 400) {
      let newsObject = new Object();
      newsObject.failedUrl = url;
      newsObject.failedUrlType = "MAIN-PAGE";
      newsObject.failStatusCode = err.response.status;
      saveDataToSotrageAzureCosmos(
        newsObject,
        databaseName,
        newsErrorCollection
      );
    }
  }
}
async function findNesHeadlinesRecursively(
  url,
  page,
  brokenLinkPropagationAllowed,
  currentBrokenLinkCount,
  fetchOnlyLatest
) {
  // log(
  //   `Current URL Base : ${url} --- Current Page - ${page} ---Error Threshold - ${brokenLinkPropagationAllowed}--Current ERROR Limit Now ---${currentBrokenLinkCount}`
  // );
  currentPageMapper.set(url, page);
  const baseUrl = "https://thenextweb.com";
  let childUrlsForParsing = new Map();
  let allowedClasses = new Set(["title_link"]);
  let allowScrolling = new Set(["Load more", "Next Page"]);
  let isMorePageAvailable = false;
  if (brokenLinkPropagationAllowed < currentBrokenLinkCount) {
    // log("lets return Back - Road closed ahead!!!");
    return;
  }
  try {
    await sleep(timeOutDuration());
    log(`Current URL Base : ${url + "/page/" + page}`);
    const { data } = await axios.get(url + "/page/" + page, {
      timeout: timeOutDuration(),
    });
    const $ = cheerio.load(data);
    const headerUrls = $("a");

    headerUrls.each(function (idx, el) {
      if (allowedClasses.has($(el).attr("class"))) {
        childUrlsForParsing.set(
          $(el).text().trim(),
          baseUrl + $(el).attr("href")
        );
      }
      if (allowScrolling.has($(el).text())) {
        isMorePageAvailable = true;
      }
    });
    if (childUrlsForParsing.size > 0) {
      processNewsSetAndStore(childUrlsForParsing);
    }
    if (isMorePageAvailable && !fetchOnlyLatest) {
      //  log("More Pages available to Parse -Lets Go to Next Page");
      findNesHeadlinesRecursively(
        url,
        page + 1,
        brokenLinkPropagationAllowed,
        0, //Resetting the Count as One success happens,
        fetchOnlyLatest
      );
    } else {
      //   log(
      //     "This is the end of the Page Scrapping - Let's Return Back || Only Scrapping Latest News Only -- [fetchOnlyLatest] = " +
      //       fetchOnlyLatest
      //   );
      return;
    }
  } catch (err) {
    // log(
    //   "Not Able to Process Headline Pagination Parser So Storing for Retry Later ====> " +
    //     url +
    //     "/page/" +
    //     page
    // );
    if (err.response && err.response.status >= 400) {
      let newsObject = new Object();
      newsObject.failedUrl = url + "/page/" + page;
      newsObject.failedUrlType = "HEADLINE-PAGES";
      newsObject.failStatusCode = err.response.status;
      saveDataToSotrageAzureCosmos(
        newsObject,
        databaseName,
        newsErrorCollection
      );
    }
    findNesHeadlinesRecursively(
      url,
      page + 1,
      brokenLinkPropagationAllowed,
      ++currentBrokenLinkCount,
      fetchOnlyLatest
    );
  }
}

async function processNewsSetAndStore(childUrlsForParsing) {
  // log(`Asynchoronously Process the Nes Set and Ste in Persistance Storage`);
  for (let [key, value] of childUrlsForParsing) {
    log(`Processing News from Url : ${value}`);
    try {
      await sleep(timeOutDuration());
      const { data } = await axios.get(value, {
        timeout: timeOutDuration(),
      });
      let wholePage = cheerio.load(data);
      let newsBody = wholePage(".c-richText.c-richText--large").html();
      let newsPublishFooter = wholePage(".c-article__pubDate>div>time");
      let newsRelatedTags = wholePage(
        ".c-header__tags.c-tags.c-tags--left>.c-tags__tag>.c-tags__link.c-article-leadtag"
      );
      let tagList = new Set();
      newsRelatedTags.each((index, item) => {
        let tagName =
          wholePage(item).attr("data-event-label") != undefined
            ? wholePage(item).attr("data-event-label")
            : wholePage(item).html();
        tagList.add(tagName);
      });
      let publishTime = newsPublishFooter.attr("datetime");
      let parsedParas = cheerio.load(newsBody);
      //Clear Out unnecessary tags
      parsedParas("img").remove();
      parsedParas("noscript").remove();
      newsBody = parsedParas.html();
      parsedParas = cheerio.load(newsBody);
      let rawParas = parsedParas("p,h2");
      let fullNewsBody = "";
      rawParas.each((index, item) => {
        if (parsedParas(item).text().trim().length > 0) {
          fullNewsBody = fullNewsBody.concat(parsedParas(item).text().trim());
          fullNewsBody = fullNewsBody.concat("\n");
        }
      });
      let newsObject = new Object();
      newsObject.datePublished = publishTime;
      newsObject.newsArticle = fullNewsBody;
      newsObject.relatedTags = Array.from(tagList);
      newsObject.headLine = key;
      newsObject.sourceUrl = value;

      saveDataToSotrageAzureCosmos(newsObject, databaseName, newsCollection);
    } catch (err) {
      //   log(
      //     "Not Able to Process The News Article So Storing for Retry Later ====>" +
      //       value
      //   );
      if (err.response && err.response.status >= 400) {
        let newsObject = new Object();
        newsObject.failedUrl = value;
        newsObject.failedUrlType = "ARTICLE";
        newsObject.failStatusCode = err.response.status;
        saveDataToSotrageAzureCosmos(
          newsObject,
          databaseName,
          newsErrorCollection
        );
      }
    }
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
