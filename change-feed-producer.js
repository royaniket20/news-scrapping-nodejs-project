// Loading the dependencies.
const axios = require("axios");
const cheerio = require("cheerio");
const { log } = require("console");
const { MongoClient } = require("mongodb");

async function saveNewsToSotrageAzureCosmos(newsPayload) {
  log("INIT TO THE CLIENT");
  const client = new MongoClient(
    "mongodb://prospect-plotter-cosmos-mongo:6QPBm4tpNWbBkjYMgn0eZfqpQAaKYLgfN76yYcIgKcrFPulgwsGs2ZE8c3VoNRxXD3rHpmwcjaROBKZwLJhW9A%3D%3D@prospect-plotter-cosmos-mongo.mongo.cosmos.azure.com:10255/?ssl=true&retrywrites=false&maxIdleTimeMS=120000&appName=@prospect-plotter-cosmos-mongo@"
  );
  // const client = new MongoClient(uriOfMongo, {
  //   useNewUrlParser: true,
  //   useUnifiedTopology: true,
  // });
  try {
    log("CONNECTING TO THE CLIENT");
    await client.connect();
    const database = client.db("world-news-scrapping");
    const news = database.collection("news");
    // Query for a Change stream
    await changeStreamWatch(news);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
    log("CLOSING THE CLIENT");
  }
}

//saveNewsToSotrageAzureCosmos({});

async function changeStreamWatch(collection) {
  var cursor = collection.watch(
    [
      { $match: { operationType: { $in: ["insert", "update", "replace"] } } },
      { $project: { _id: 1, fullDocument: 1, ns: 1, documentKey: 1 } },
    ],
    { fullDocument: "updateLookup" }
  );
  console.log("---Reached Here ---------");

  while (cursor.hasNext()) {
    cursor.next();
  }
}

const { EventHubProducerClient } = require("@azure/event-hubs");

const connectionString =
  "Endpoint=sb://prospect-plotter-event-hub-for-world-news.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=Jb7nddyzMPt9rm3SXUe9qjdD2j+7qRiyh1OQcBmtABI=";
const eventHubName = "news-feed-hub";

async function main() {
  // Create a producer client to send messages to the event hub.
  const producer = new EventHubProducerClient(connectionString, eventHubName);

  // Prepare a batch of three events.
  const batch = await producer.createBatch();
  batch.tryAdd({ body: "First event" });
  batch.tryAdd({ body: "Second event" });
  batch.tryAdd({ body: "Third event" });

  // Send the batch to the event hub.
  await producer.sendBatch(batch);

  // Close the producer client.
  await producer.close();

  console.log("A batch of three events have been sent to the event hub");
}

main().catch((err) => {
  console.log("Error occurred: ", err);
});
